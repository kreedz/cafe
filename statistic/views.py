from django.shortcuts import render
from django.views.generic import View

from django.db.models import Sum, F, Count
from django.db.models.functions import TruncMonth

from django.contrib.auth.mixins import UserPassesTestMixin

from order.models import Order, OrderItem


def get_statistics():
        from datetime import datetime
        by_days_in_month = Order.objects\
            .filter(date__month=datetime.today().month)\
            .values('date')\
            .annotate(sum_=Sum(
                F('orderitem__food__price') * F('orderitem__count'))
            )
        by_days_in_month_count = Order.objects\
            .filter(date__month=datetime.today().month)\
            .values('date').annotate(c=Count('date'))
        for i, order in enumerate(by_days_in_month):
            by_days_in_month[i].update(by_days_in_month_count[i])

        by_months_in_year = OrderItem.objects.values('order__date')\
            .filter(order__date__year=datetime.today().year)\
            .annotate(month=TruncMonth('order__date')).values('month')\
            .annotate(sum_=Sum(F('food__price')*F('count')))
        by_months_in_year_count = OrderItem.objects.values('order__date')\
            .filter(order__date__year=datetime.today().year)\
            .annotate(date=TruncMonth('order__date')).values('date')\
            .annotate(c=Count('count'))
        for i, order in enumerate(by_months_in_year):
            by_months_in_year[i].update(by_months_in_year_count[i])

        food_count_by_day_month = OrderItem.objects.values('food__name')\
            .annotate(sum_count=Sum('count'))
        food_day = datetime.today().day
        food_month = datetime.today().month
        return {
            'by_days_in_month': by_days_in_month,
            'by_months_in_year': by_months_in_year,
            'food_count_by_day': food_count_by_day_month.filter(
                order__date__day=food_day
            ),
            'food_count_by_month': food_count_by_day_month.filter(
                order__date__month=food_month
            ),
            'food_day': food_day,
            'food_month': food_month,
        }


class Statistic(UserPassesTestMixin, View):
    login_url = '/admin'

    def test_func(self):
        return self.request.user.is_staff

    def get(self, request, *args, **kwargs):
        context = get_statistics()
        return render(self.request, 'statistic/stat.html', context=context)
