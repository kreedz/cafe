# -*- coding: utf-8 -*
from django.core.management.base import BaseCommand, CommandError

from statistic.views import get_statistics

from openpyxl import Workbook


def write_statistics(metas):
    ws = metas['ws']
    for meta in metas['meta']:
        statistic = metas['statistics'][meta['name']]
        if statistic:
            i = metas['i']
            ws.cell(row=i+2, column=1).value = meta['title']
            for j, title_field in enumerate(meta['title_fields'], start=2):
                ws.cell(row=i+3, column=j).value = title_field
            for l, order in enumerate(statistic, start=i+4):
                for j, field in enumerate(meta['fields'], start=2):
                    if 'fn' in meta:
                        fn = meta['fn']
                        if fn and fn['field'] == field:
                            order[field] = fn['fn'](order[field])
                    ws.cell(row=l, column=j).value = order[field]
            metas['i'] = l


class Command(BaseCommand):
    help = 'Export statistics from database to xls file'

    def add_arguments(self, parser):
        parser.add_argument(
            '--file',
            action='store',
            dest='filename',
            default=False,
            help='specify import file',
        )

    def handle(self, *args, **options):
        import locale
        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')

        filename = options['filename']
        if not filename:
            raise CommandError('Option `--file=...` must be specified.')

        statistics = get_statistics()
        wb = Workbook()
        ws = wb.active

        metas = {
            'statistics': statistics,
            'ws': ws,
            'i': 1,
            'meta': ({
                'name': 'by_days_in_month',
                'title': (
                    'Сколько заказов и на какую сумму'
                    'было сделано по дням за месяц'
                ),
                'title_fields': ['Дата', 'Сумма', 'Количество'],
                'fields': ['date', 'sum_', 'c'],
            }, {
                'name': 'by_months_in_year',
                'title': (
                    'Сколько заказов и на какую сумму'
                    'было сделано по месяцам за год'
                ),
                'title_fields': ['Месяц', 'Сумма', 'Количество'],
                'fields': ['date', 'sum_', 'c'],
                'fn': {
                    'field': 'date',
                    'fn': lambda date: date.strftime('%B'),
                },
            }, {
                'name': 'food_count_by_day',
                'title': (
                    'Сколько каких блюд было заказано за день'
                ),
                'title_fields': ['Блюдо', 'Сумма'],
                'fields': ['food__name', 'sum_count'],
            }, {
                'name': 'food_count_by_month',
                'title': (
                    'Сколько каких блюд было заказано за месяц'
                ),
                'title_fields': ['Блюдо', 'Сумма'],
                'fields': ['food__name', 'sum_count'],
            }),
        }

        write_statistics(metas)

        ws.column_dimensions['B'].width = 20
        wb.save(filename)

        self.stdout.write(
            self.style.SUCCESS(
                'Successfully exported to file "%s"' % filename))
