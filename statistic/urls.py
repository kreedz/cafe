from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'stat', views.Statistic.as_view(), name='stat'),
]
