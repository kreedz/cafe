(function(){
    var dates = [];
    $('ul:first>li').each(function(){
        dates.push($(this).attr('data-date'));
    });
    if (!dates.length) return;
    var sums = [];
    $('ul:first ul').find('li:first').each(function(){
        sums.push($(this).attr('data-sum'));
    });

    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels : dates,
            datasets : [{
                label: 'График по оси Y сумма заказов за день, по оси X дни за один месяц',
                fillColor : 'rgba(172,194,132,0.4)',
                strokeColor : '#ACC26D',
                pointColor : '#fff',
                pointStrokeColor : '#9DB86D',
                data : sums,
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
})();
