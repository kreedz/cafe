$(document).ready(function(){
    function sendNotification(title, options){
        // Проверим, поддерживает ли браузер HTML5 Notifications
        if (!("Notification" in window)) {
            alert('Ваш браузер не поддерживает HTML Notifications, его необходимо обновить.');
        }
        // Проверим, есть ли права на отправку уведомлений
        else if (Notification.permission === "granted") {
            // Если права есть, отправим уведомление
            var notification = new Notification(title, options);

            function clickFunc(){
                alert('Пользователь кликнул на уведомление');
            }

            notification.onclick = clickFunc;
        }
        // Если прав нет, пытаемся их получить
        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                // Если права успешно получены, отправляем уведомление
                if (permission === "granted") {
                    var notification = new Notification(title, options);
                } else {
                    alert('Вы запретили показывать уведомления'); // Юзер отклонил наш запрос на показ уведомлений
                }
            });
        } else {
        // Пользователь ранее отклонил наш запрос на показ уведомлений
        // В этом месте мы можем, но не будем его беспокоить. Уважайте решения своих пользователей.
        }
    }

    var csrftoken = Cookies.get('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    (function loopGettingOrders(id){
        if (id) {
            setTimeout(function(){
                $.get('new/' + id, function(data){
                    if (data.is_new_orders) {
                        sendNotification('Появился новый заказ!', {
                            body: '',
                            dir: 'auto',
                        });
                    }
                    loopGettingOrders(data.id_latest);
                });
            }, 3000);
        }
    })($('.order-info:last li:first').attr('data-id'));

});
