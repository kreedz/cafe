# -*- coding: utf-8 -*
from __future__ import unicode_literals

from django.db import models


class OrderItem(models.Model):
    order = models.ForeignKey('Order')
    food = models.ForeignKey('menu.Food')
    count = models.IntegerField(verbose_name='Количество', default=1)

    class Meta:
        verbose_name = 'Элемент заказа'
        verbose_name_plural = 'Элементы заказа'

    def __unicode__(self):
        return 'Food: {0}, Count: {1}'.format(self.food.name, self.count)


class Order(models.Model):
    time = models.TimeField(
        verbose_name='Время', auto_now_add=True)
    date = models.DateField(
        verbose_name='Дата', auto_now_add=True)
    session = models.CharField(
        verbose_name='Сессия', editable=False, max_length=40, null=True)
    user = models.ForeignKey('auth.User', null=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __unicode__(self):
        if self.user:
            user = self.user.username
        else:
            user = self.session
        return 'User: {0}, date: {1}, time: {2}'.format(
            user, self.date, self.time)


class Report(models.Model):
    pass
