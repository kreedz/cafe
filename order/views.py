# -*- coding: utf-8 -*
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.generic import View

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from menu.models import Food
from .models import Order, OrderItem

import json


class JSONResponseMixin(object):

    def render_to_json_response(self, context, **response_kwargs):
        return JsonResponse(
            self.get_data(context), safe=False,
            **response_kwargs
        )

    def get_data(self, context):
        return context


class UpdateBasketItemsInSessionView(View):

    def post(self, request):
        orderjs = json.loads(request.body)
        request.session['order'] = orderjs

        return HttpResponse(status=200)


class PostOrderView(View):

    def post(self, request):
        orderjs = json.loads(request.body)
        if request.user.is_authenticated():
            order = Order(user=request.user)
        else:
            order = Order(session=request.session.session_key)
        order.save()
        sum_ = 0
        for number, item in orderjs.iteritems():
            food = Food.objects.get(
                name=item['description'], price=item['priceForOne'])
            OrderItem(order=order, food=food, count=item['count']).save()
            sum_ += int(item['price'])
        context = {'order_items': orderjs, 'order_sum': sum_, 'order': order}
        return render(self.request, 'order/check.html', context=context)


class Orders(LoginRequiredMixin, View):
    login_url = '/admin'

    def get(self, request):
        orders = Order.objects.filter(user=self.request.user)
        context_orders = {}
        for i, order in enumerate(orders):
            order_items = order.orderitem_set.all()
            context_orders[i] = {
                'order': order, 'order_items': order_items
            }
        return render(
            self.request,
            'order/orders.html', context={'orders': context_orders}
        )


class OrdersDaily(UserPassesTestMixin, View):
    login_url = '/admin'

    def test_func(self):
        return self.request.user.is_staff

    def get(self, request, *args, **kwargs):
        from datetime import date
        orders = Order.objects.filter(date=date.today())
        context_orders = {}
        orders_sum = 0
        for i, order in enumerate(orders, start=1):
            order_sum = 0
            order_items = order.orderitem_set.all()
            for j, order_item in enumerate(order_items):
                order_sum += order_item.food.price * order_item.count
            orders_sum += order_sum
            context_orders[i] = {
                'order': order, 'order_items': order_items, 'sum': order_sum,
            }
        return render(
            self.request, 'order/orders.html', context={
                'orders': context_orders,
                'orders_sum': orders_sum,
                'orders_count': i,
                'is_daily_orders': True,
            }
        )


class Check(UserPassesTestMixin, View):
    login_url = '/admin'

    def test_func(self):
        return self.request.user.is_staff

    def get(self, request, *args, **kwargs):
        order = Order.objects.get(id=self.kwargs['id'])
        order_items_qs = order.orderitem_set.all()
        order_items = {}
        sum_ = 0
        for i, order_item in enumerate(order_items_qs):
            price = order_item.food.price * order_item.count
            order_items[i] = {
                'description': order_item.food.name,
                'count': order_item.count,
                'priceForOne': order_item.food.price,
                'price': price,
            }
            sum_ += price
        context = {
            'order_items': order_items,
            'order_sum': sum_,
            'order': order,
        }
        return render(self.request, 'order/check.html', context=context)


class CheckNewOrders(JSONResponseMixin, UserPassesTestMixin, View):
    login_url = '/admin'

    def test_func(self):
        return self.request.user.is_staff

    def get(self, request, *args, **kwargs):
        is_new_orders = False
        if Order.objects.filter(id__gt=self.kwargs['id']):
            is_new_orders = True
        context = {
            'is_new_orders': is_new_orders,
            'id_latest': Order.objects.latest('id').id,
        }
        return self.render_to_json_response(context=context)
