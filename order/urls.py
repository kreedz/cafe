from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'post-order-session',
        views.UpdateBasketItemsInSessionView.as_view(),
        name='post_order_session'),
    url(r'post-order-db', views.PostOrderView.as_view(), name='post_order_db'),
    url(r'^orders$', views.Orders.as_view(), name='orders'),
    url(r'^dayli$', views.OrdersDaily.as_view(), name='orders_daily'),
    url(r'check/(?P<id>\d+)', views.Check.as_view(), name='check'),
    url(r'new/(?P<id>\d+)',
        views.CheckNewOrders.as_view(), name='check_new_orders'),
]
