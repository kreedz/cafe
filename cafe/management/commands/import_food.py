# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from menu.models import Food, Category

import csv
import os
import shutil
import xml.etree.ElementTree as ET


def get_keyerror_msg(filename, keys):
    msg = u'Неверное название поля в файле {0}\n'
    msg += u'Названия полей должны быть: '
    msg += u', '.join(keys)
    return msg.format(filename)


def get_data_from_file(ext, filename, keys):
    data = []
    if ext == 'csv':
        reader = csv.DictReader(
            open(filename, 'r'), delimiter=';')
        for row in reader:
            try:
                food = {}
                food['category_name'] = row['Категория']
                food['name'] = row['Блюдо']
                food['price'] = row['Цена']
                food['image'] = os.path.join('images', row['Картинка'])
            except KeyError:
                print get_keyerror_msg(filename, keys)
                raise
            data.append(food)
        return data
    elif ext == 'xml':
        with open(filename) as f:
            root = ET.fromstring(f.read())
            keys.append(u'food')
            try:
                rows = root.findall('.//food')
                for row in rows:
                    food = {}
                    food['category_name'] = row.find(u'Категория').text
                    food['name'] = row.find(u'Блюдо').text
                    food['price'] = row.find(u'Цена').text
                    food['image'] = os.path.join(
                        'images', row.find(u'Картинка').text)
                    data.append(food)
            except AttributeError:
                raise CommandError(get_keyerror_msg(filename, keys))

    return data


def save2db(data):
    for food in data:
        category, created = Category.objects.get_or_create(
            name=food['category_name'])

        food, created = Food.objects.update_or_create(
            name=food['name'], category=category,
            defaults={
                'price': food['price'], 'image': food['image'],
            }
        )


class Command(BaseCommand):
    help = 'Export food from file'

    def add_arguments(self, parser):
        parser.add_argument(
            '--file',
            action='store',
            dest='filename',
            default=False,
            help='specify import file',
        )

    def handle(self, *args, **options):
            # some options validations
            filename = options['filename']
            if not filename:
                raise CommandError('Option `--file=...` must be specified.')
            if not os.path.isfile(filename):
                raise CommandError(
                    'File does not exist at the specified path.')
            extension = os.path.splitext(filename)[1][1:].lower()
            if not (extension == 'csv' or extension == 'xml'):
                raise CommandError('File must be either csv or xml!')
            images_path = 'images'
            images_path = os.path.join(
                os.path.dirname(
                    os.path.abspath(filename)), images_path)
            msg = 'The directory "images" must exist beside the file {0}.'
            if not os.path.isdir(images_path):
                raise CommandError(msg.format(filename))

            # extract data from file and save to db
            keys = [u'Блюдо', u'Категория', u'Цена', u'Картинка']
            data = get_data_from_file(extension, filename, keys)
            save2db(data)

            # copy images from outside directory to app/media/images/
            dest_imgs_dir = os.path.abspath(os.path.join('media', 'images'))
            for image in os.listdir(images_path):
                shutil.copy(os.path.join(images_path, image), dest_imgs_dir)

            self.stdout.write(
                self.style.SUCCESS(
                    'Successfully imported file "%s"' % filename))
