# -*- coding: utf-8 -*-
from django.db import models


class Food(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100)
    category = models.ForeignKey('Category')
    price = models.IntegerField(verbose_name='Цена')
    image = models.ImageField(verbose_name='Изображение', upload_to='images')

    class Meta:
        unique_together = ('name', 'category')
        verbose_name = 'Блюдо'
        verbose_name_plural = 'Блюда'

    def __unicode__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(
        verbose_name='Название', max_length=100, unique=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __unicode__(self):
        return self.name
