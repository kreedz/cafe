# -*- coding: utf-8 -*
from django.views.generic import ListView
from models import Food


class FoodListView(ListView):
    template_name = u'menu/index.html'
    context_object_name = 'food_items'

    def get_queryset(self):
        return Food.objects.order_by('category')
