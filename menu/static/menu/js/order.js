(function(){
    var csrftoken = Cookies.get('csrftoken');
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    function postOrderData(store) {
        var order = {};
        $('.calculator tbody tr:not([hidden]):not(:last)').each(function(){
            var number = $(this).find('th').text();
            var description = $(this).find('.calc-description').text();
            var count = $(this).find('input').val();
            var price = $(this).find('.calc-price').text();
            order[number] = {
                'description': description,
                'count': count,
                'price': price,
                'priceForOne': price / count,
            };
        });

        var args = {
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: '/order/post-order-' + (store == 'session' ? store : 'db'),
            data: JSON.stringify(order),
        };
        if (store == 'db') {
            args['success'] = function(data){
                var win = window.open();
                win.document.write(data);
            }
            clearBasket();
        }
        $.ajax(args);
    }

    function updateSum(value) {
        var sumDiv = $('.calculator tr:last th:last');
        var sum = 0;
        var prices = $('.calculator').find('tr:not([hidden]) .calc-price');
        if (prices.length) {
            prices.each(function(){
                sum += parseInt($(this).text());
            });
            sumDiv.text(sum);
        }
    }

    function recountOrderNumbers() {
        $('.calculator').find('tr:not([hidden]):not(:last):not(:first) th').each(function(index){
            $(this).text(index + 1);
        });
    }

    function getBasketItems() {
        return $('.calculator tbody tr:not([hidden]):not(:last)');
    }

    function clearBasket() {
        getBasketItems().remove();
        $('.calculator tr:last th:last').text('');
        postOrderData('session');
    }

    $('#calc-clear').on('click', function(){
        clearBasket();
    });

    $('#calc-order').on('click', function(){
        if (getBasketItems().length) {
            postOrderData('db');
        }
    });

    var trCountsWithOneFood = 2;
    $('.calculator tr').find('td:last').on('click', function(){
        var tr = $(this).parent();

        var sum = $('.calculator tr:last th:last');
        sum.text(sum.text() - tr.find('.calc-price').text());
        if (tr.siblings().length == trCountsWithOneFood) {
            tr.parent().find('tr:last th:last').text('');
        }
        tr.remove();
        recountOrderNumbers();
        postOrderData('session');
    });

    $('.item').on('click', function(){
        var price = $(this).find('.item-price').text();
        var description = $(this).find('.item-description').text();
        var itemNumb = $('.calculator tr').length - trCountsWithOneFood;
        var isInserted = false;
        $('.calculator tr:not([hidden]) .calc-description').each(function(){
            if ($(this).text() == description) {
                var calcCount = $(this).parent().find('input');
                var calcPrice = $(this).siblings('.calc-price');
                calcCount.val(parseInt(calcCount.val()) + 1);
                calcPrice.text(price * calcCount.val());

                postOrderData('session');
                isInserted = true;
            }
        });

        updateSum();

        if (isInserted) {
            return;
        }

        var calcItem = $('.calculator tr[hidden]').clone(true, true);
        calcItem.removeAttr('hidden');
        calcItem.find('th').text(itemNumb);
        calcItem.find('.calc-description').text(description);
        calcItem.find('.calc-price').text(price);
        calcItem.find('.calc-price').attr('data-price', price);
        calcItem.insertBefore($('.calculator tr:last'));
        postOrderData('session');
        updateSum();
    });

    $('.calculator input').on('change', function(){
        var calcPrice = $(this).closest('tr').find('.calc-price');
        var priceForOne = $(this).closest('tr').find('.calc-price').attr('data-price');
        calcPrice.text(priceForOne * $(this).val());
        updateSum();
        postOrderData('session');
    });

    updateSum();
})();
